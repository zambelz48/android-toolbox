package zambelz.toolbox.library.tabsviewpager;

import zambelz.toolbox.library.tabsviewpager.ZafSlidingTabsViewPagerAdapter.ZafTabsFragmentItems;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.PageTransformer;

@SuppressLint("NewApi")
public class ZafSlidingTabsViewPager implements ActionBar.TabListener {

	private ActionBar actionBar;
	private FragmentActivity activity;
	private int pagerId;
	private ViewPager viewPager;
	private String[] tabsTitle;
	private int[] tabsIconUnselected;
	private int[] tabsIconSelected;
	
	public ZafSlidingTabsViewPager(FragmentActivity activity, int pagerId) {
		this.activity = activity;
		actionBar = this.activity.getActionBar();
		this.pagerId = pagerId;
	}
	
	public void setTitle(String[] tabsTitle) {
		this.tabsTitle = tabsTitle;
	}
	
	private String getTitle(int position) {
		if(tabsTitle != null) {
			if(tabsTitle[position] != null) {
				return tabsTitle[position];
			} else {
				return "";
			}
		} else {
			return "";
		}
	}
	
	public void setIconSelected(int[] tabsIcon) {
		this.tabsIconSelected = tabsIcon;
	}
	
	private String getIconSelected(int position) {
		if(tabsIconSelected != null) {
			if(String.valueOf(tabsIconSelected[position]) != null || tabsIconSelected[position] != 0){
				return String.valueOf(tabsIconSelected[position]);
			} else {
				return getIconUnselected(position);
			}
		} else {
			return getIconUnselected(position);
		}
	}
	
	public void setIconUnselected(int[] tabsIcon) {
		this.tabsIconUnselected = tabsIcon;
	}
	
	private String getIconUnselected(int position) {
		if(tabsIconUnselected != null) {
			if(String.valueOf(tabsIconUnselected[position]) != null || tabsIconUnselected[position] != 0) {
				return String.valueOf(tabsIconUnselected[position]);
			} else {
				return "";
			}
		} else {
			return "";
		}
	}
	
	private int getTabsLength() {
		if(tabsIconUnselected != null) {
			return tabsIconUnselected.length;
		} else if(tabsTitle != null) {
			return tabsTitle.length;
		} else {
			setTitle(new String[]{"Unselected Icon or Title Is not set correctly"});
			return tabsTitle.length;
		}
	}
	
	private void setOnFirstBuildTabs() {
		for (int i = 0; i < getTabsLength(); i++) {
			if(tabsIconUnselected != null) {
				if(String.valueOf(tabsIconUnselected[i]) != null) {
					actionBar.addTab(actionBar.newTab()
							.setIcon(Integer.parseInt(getIconUnselected(i)))
							.setText(getTitle(i)).setTabListener(this));
				} else {
					actionBar.addTab(actionBar.newTab().setText(getTitle(i))
							.setTabListener(this));
				}
			} else {
				actionBar.addTab(actionBar.newTab().setText(getTitle(i))
						.setTabListener(this));
			}
		}
	}
	
	private void setOnSelectedTabs(int position) {
		if(tabsIconUnselected != null) {
			actionBar.getSelectedTab()
					.setIcon(Integer.parseInt(getIconSelected(position)))
					.setText(getTitle(position)).setTabListener(this);
		}
	}
	
	private void setOnUnselectedTabs(int position) {
		if(tabsIconUnselected != null) {
			actionBar.getSelectedTab()
					.setIcon(Integer.parseInt(getIconUnselected(position)))
					.setText(getTitle(position)).setTabListener(this);
		}
	}
	
	private PageTransformer transType;
	private boolean usePageTransform = false;

	public void setPageTransformer(boolean usePageTransform, PageTransformer transType) {
		this.usePageTransform = usePageTransform;
		this.transType = transType;
	}
	
	public void build(ZafTabsFragmentItems tabsItemListener) {
		viewPager = (ViewPager) activity.findViewById(pagerId);
		ZafSlidingTabsViewPagerAdapter tabsAdapter = new ZafSlidingTabsViewPagerAdapter(
				activity.getSupportFragmentManager(), getTabsLength()
		);
		tabsAdapter.setItem(tabsItemListener);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		viewPager.setAdapter(tabsAdapter);
		
		if(usePageTransform != false && transType != null) {
			viewPager.setPageTransformer(true, transType);
		}
		
		// build tabs
		setOnFirstBuildTabs();
		
        // on swiping the viewpager make respective tab selected
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				// on changing the page make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		
	}
	
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
		setOnSelectedTabs(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		setOnUnselectedTabs(tab.getPosition());
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {}

}
