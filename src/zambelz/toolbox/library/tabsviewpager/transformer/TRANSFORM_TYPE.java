package zambelz.toolbox.library.tabsviewpager.transformer;

public class TRANSFORM_TYPE {

	public static StackTransformer Stack() {
		try {
			return StackTransformer.class.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static CubeOutTransformer CubeOut() {
		try {
			return CubeOutTransformer.class.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static AccordionTransformer Accordion() {
		try {
			return AccordionTransformer.class.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}