package zambelz.toolbox.library.tabsviewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ZafSlidingTabsViewPagerAdapter extends FragmentPagerAdapter{
	
	private int count;
	private ZafTabsFragmentItems tabsItemListener;
	
	public ZafSlidingTabsViewPagerAdapter(FragmentManager fm, int count) {
		super(fm);
		this.count = count;
	}

	public void setItem(ZafTabsFragmentItems tabsItemListener) {
		this.tabsItemListener = tabsItemListener;
	}
	
	@Override
	public Fragment getItem(int index) {
		return tabsItemListener.onTabsItemSelectedListener(index);
	}

	@Override
	public int getCount() {
		return count;
	}
	
	public interface ZafTabsFragmentItems {
		public Fragment onTabsItemSelectedListener(int index);
	}

}
