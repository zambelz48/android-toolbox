package zambelz.toolbox.library;

/**
 * ZAMBELZ FRAMEWORK Open Source Project
 * SOAP UTILITY based KSOAP LIBRARY
 * KSOAP Libs Version = ksoap2-android-assembly-3.2.0-jar-with-dependencies.jar 
 * @author Nanda . J . A 
 * @version 03.05.2014 build 14.06 
 */

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class ZafSOAPUtility {
	
	public static final int VER10 = SoapEnvelope.VER10;
	public static final int VER11 = SoapEnvelope.VER11;
	public static final int VER12 = SoapEnvelope.VER12;
	
	private SoapObject reqRetrieve 				= null;
	private SoapSerializationEnvelope envelope	= null;
	private HttpTransportSE httpTransport 		= null;
	private PropertyInfo pInfo					= null;
	
	private int timeout = 20000;
	
	public ZafSOAPUtility(int SoapEnvelopeVersion) {
		envelope = new SoapSerializationEnvelope(SoapEnvelopeVersion);
	}
	
	/**
	 * init for Custom Web Service
	 * @param int SoapEnvelopeVersion
	 * @param boolean implicitTypes
	 * @param String URL
	 */
	public void init(boolean implicitTypes, String URL) {
		envelope.implicitTypes	= implicitTypes;
		envelope.dotNet 		= false;
		httpTransport 			= new HttpTransportSE(URL, getTimeout());
	}
	
	/**
	 * init for .NET Web Service
	 * @param int SoapEnvelopeVersion
	 * @param boolean implicitTypes
	 * @param String URL
	 */
	public void initNET(boolean implicitTypes, String URL) {
		envelope.implicitTypes	= implicitTypes;
		envelope.dotNet 		= true;
		httpTransport 			= new HttpTransportSE(URL, getTimeout());
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	/**
	 * init soap method
	 * @param String server
	 * @param String method
	 * @return
	 */
	public SoapObject initMethod(String server, String method) {
		reqRetrieve = new SoapObject(server, method);
		return reqRetrieve;
	}
	
	/**
	 * set output soap object
	 */
	private void setOutputObject() {
		envelope.setOutputSoapObject(reqRetrieve);
	}
	
	/**
	 * 
	 * @param String soapAction
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public void get(String soapAction) throws IOException, XmlPullParserException {
		setOutputObject();
		envelope.bodyOut = reqRetrieve;
		httpTransport.debug = true;
		httpTransport.call(soapAction, envelope);
	}
	
	/**
	 * 
	 * @param String soapAction
	 * @param boolean debug
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public void get(String soapAction, boolean debug) throws IOException, XmlPullParserException {
		setOutputObject();
		envelope.bodyOut = reqRetrieve;
		httpTransport.debug = true;
		httpTransport.call(soapAction, envelope);
	}
	
	/**
	 * 
	 * @param String soapAction
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public void post(String soapAction) throws IOException, XmlPullParserException {
		setOutputObject();
		httpTransport.debug = true;
		httpTransport.call(soapAction, envelope);
	}
	
	/**
	 * 
	 * @param String soapAction
	 * @param debug
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	public void post(String soapAction, boolean debug) throws IOException, XmlPullParserException {
		setOutputObject();
		httpTransport.debug = debug;
		httpTransport.call(soapAction, envelope);
	}
	
	/**
	 * set is debugable
	 * @param boolean debug
	 * @return
	 */
	public HttpTransportSE setDebug(boolean debug) {
		httpTransport.debug = debug;
		return httpTransport;
	}
	
	/**
	 * return soap return
	 * @return
	 * @throws SoapFault
	 */
	public SoapObject getResponse() throws SoapFault {
		return (SoapObject) envelope.getResponse();
	}
	
	/**
	 * 
	 * @return
	 * @throws SoapFault
	 */
	public SoapObject getTable() throws SoapFault {
		SoapObject responseBody = null;
		
		responseBody = (SoapObject) envelope.getResponse();
		responseBody = (SoapObject) responseBody.getProperty(1);
		
		return (SoapObject) responseBody.getProperty(0);
	}
	
	/**
	 * get soap row
	 * @param int i
	 * @return
	 * @throws SoapFault
	 */
	public SoapObject getRow(int i) throws SoapFault {
		return (SoapObject) getTable().getProperty(i);
	}
	
	/**
	 * get string soap value by row 
	 * @param int i
	 * @param String value
	 * @return return string soap value by row
	 * @throws SoapFault
	 */
	public String getRowString(int i, String value) throws SoapFault {
		return getRow(i).getProperty(value).toString();
	}
	
	/**
	 * get string soap property value
	 * @param String value
	 * @return String soap value
	 * @throws SoapFault
	 */
	public String getProperty(String value) throws SoapFault {
		SoapObject val = (SoapObject) getTable().getProperty(value);
		return val.toString();
	}
	
	/** 
	 * @return total soap data
	 * @throws SoapFault
	 */
	public int getPropertyCount() throws SoapFault {
		return getTable().getPropertyCount();
	}
	
	/**
	 * set property string value 
	 * @param String sName
	 * @param String sValue
	 * @param Object type
	 * @return
	 */
	public SoapObject setProperty(String sName, String sValue) {
		pInfo = new PropertyInfo();
		pInfo.setName(sName);
		pInfo.setValue(sValue);
		pInfo.setType(String.class);
		
		return reqRetrieve.addProperty(pInfo);
	}
	
	/**
	 * set property double value 
	 * @param String sName
	 * @param Double sValue
	 * @param Object type
	 * @return
	 */
	public SoapObject setProperty(String sName, Double sValue) {
		pInfo = new PropertyInfo();
		pInfo.setName(sName);
		pInfo.setValue(sValue);
		pInfo.setType(Double.class);
		
		return reqRetrieve.addProperty(pInfo);
	}
	
	/**
	 * set property integer value 
	 * @param String sName
	 * @param int sValue
	 * @param Object type
	 * @return
	 */
	public SoapObject setProperty(String sName, int sValue) {
		pInfo = new PropertyInfo();
		pInfo.setName(sName);
		pInfo.setValue(sValue);
		pInfo.setType(Integer.class);
		
		return reqRetrieve.addProperty(pInfo);
	}
	
	/**
	 * set property byte value
	 * @param String sName
	 * @param byte[] sValue
	 * @param Object type
	 * @return
	 */
	public SoapObject setProperty(String sName, byte[] sValue) {
		pInfo = new PropertyInfo();
		pInfo.setName(sName);
		pInfo.setValue(sValue);
		pInfo.setType(byte.class);
		
		return reqRetrieve.addProperty(pInfo);
	}
	
	/**
	 * set property by string array
	 * @param String[] sName
	 * @param String[] sValue
	 */
	public void setProperty(String[] sName, String[] sValue) {
		for(int i = 0; i < sName.length; i++) {
			setProperty(sName[i], sValue[i]);
		}
	}
	
	/**
	 * @return soap result/response
	 * @throws SoapFault
	 */
	public SoapPrimitive result() throws SoapFault {
		return (SoapPrimitive) envelope.getResponse();
	}

}