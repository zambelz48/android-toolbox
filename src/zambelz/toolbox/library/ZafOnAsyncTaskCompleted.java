package zambelz.toolbox.library;

/**
 * ZambelzToolbox Open Source Project
 * @author Nanda . J . A  
 */

public interface ZafOnAsyncTaskCompleted {

	public void onTaskCompleted();
	
}