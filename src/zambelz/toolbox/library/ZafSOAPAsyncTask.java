package zambelz.toolbox.library;

import android.content.Context;

public abstract class ZafSOAPAsyncTask extends ZafAsyncTask {

	private ZafSOAPUtility soap;
	
	protected ZafSOAPAsyncTask(int SoapEnvelopeVersion, Context context, ZafOnAsyncTaskCompleted task) {
		super(context, task);
		soap = new ZafSOAPUtility(SoapEnvelopeVersion);
	}
	
	protected ZafSOAPAsyncTask(int SoapEnvelopeVersion, Context context) {
		super(context, null);
		soap = new ZafSOAPUtility(SoapEnvelopeVersion);
	}
	
	protected ZafSOAPAsyncTask(Context context, ZafOnAsyncTaskCompleted task) {
		super(context, task);
		soap = new ZafSOAPUtility(ZafSOAPUtility.VER11);
	}
	
	protected ZafSOAPAsyncTask(Context context) {
		super(context, null);
		soap = new ZafSOAPUtility(ZafSOAPUtility.VER11);
	}
	
	protected ZafSOAPUtility soap() {
		return soap;
	}
	
}