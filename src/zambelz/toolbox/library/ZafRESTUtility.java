package zambelz.toolbox.library;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ZafRESTUtility {

	private ZafJSONParser jParser = null;
	private JSONArray jArray = null;
	private List<NameValuePair> jParams = null;
	private JSONObject jRequest = null;
	private JSONObject jContent = null;
	
	/**
	 * Constructor
	 */
	public ZafRESTUtility() {
		jParser = new ZafJSONParser();
		jParams = new ArrayList<NameValuePair>();
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void setJParams(String key, String value) {
		jParams.add(new BasicNameValuePair(key, value));
	}
	
	/**
	 * 
	 * @param JSONUrl
	 */
	public void initGET(String JSONUrl) {
		jRequest = jParser.makeHttpRequest(JSONUrl, "GET", jParams);
	}
	
	/**
	 * 
	 * @param JSONUrl
	 */
	public void initPOST(String JSONUrl) {
		jRequest = jParser.makeHttpRequest(JSONUrl, "POST", jParams);
	}
	
	/**
	 * 
	 * @param content
	 * @throws JSONException
	 */
	public void getJArray(String content) throws JSONException {
		jArray = jRequest.getJSONArray(content);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getContentLength() {
		return jArray.length();
	}
	
	/**
	 * 
	 * @param position
	 * @throws JSONException
	 */
	public void getJObject(int position) throws JSONException {
		jContent = jArray.getJSONObject(position);
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 * @throws JSONException
	 */
	public String getJContent(String value) throws JSONException {
		return jContent.getString(value);
	}
	
	/**
	 * 
	 * @param resultKey
	 * @return
	 * @throws JSONException
	 */
	public int getJResult(String resultKey) throws JSONException {
		return jRequest.getInt(resultKey);
	}
}