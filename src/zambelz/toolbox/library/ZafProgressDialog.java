package zambelz.toolbox.library;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class ZafProgressDialog {

	private Activity activity;
	private Context context;
	private static ProgressDialog progressBar;
	
	/**
	 * Constructor
	 * @param Activity activity
	 */
	public ZafProgressDialog(Activity activity) {
		this.activity = activity;
	}
	
	/**
	 * Constructor
	 * @param Context context
	 */
	public ZafProgressDialog(Context context) {
		this.context = context;
	}
	
	/**
	 * set context type
	 */
	private void setContextType() {
		if(this.activity == null) {
			progressBar = new ProgressDialog(this.context);
		} else {
			progressBar = new ProgressDialog(this.activity);
		}
	}
	/**
	 * 
	 * @param String message
	 */
	public void setProgressDialog(String message) {
		setContextType();
		progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressBar.setCancelable(false);
		progressBar.setMessage(message);
	}
	
	/**
	 * 
	 * @param boolean isCancelable
	 * @param String message
	 */
	public void setProgressDialog(boolean isCancelable, String message) {
		setContextType();
		progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressBar.setCancelable(isCancelable);
		progressBar.setMessage(message);
	}
	
	/**
	 * 
	 * @param int setMax
	 * @param boolean isCancelable
	 * @param String message
	 */
	public void setProgressDialog(int setMax, boolean isCancelable, String message) {
		setContextType();
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setMax(setMax);
		progressBar.setCancelable(isCancelable);
		progressBar.setMessage(message);
	}
	
	public void showProgress(int progress) {
		progressBar.setProgress(progress);
	}
	
	/**
	 * show progress dialog
	 */
	public void show() {
		progressBar.show();
	}
	
	/**
	 * hide progress dialog
	 */
	public void hide() {
		progressBar.hide();
	}
	
	/**
	 * dismiss progress dialog
	 */
	public void dismiss() {
		progressBar.dismiss();
	}
	
}